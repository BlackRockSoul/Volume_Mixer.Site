/*global
$, TweenMax, ScrollMagic
*/

var controller = new ScrollMagic.Controller();

function set_parallax(parallax_object, style_name, style, parallax_led, styleLED_name, styleLED) {
    "use strict";
    var trigger_block = "#section-parallax-1",
        vh = $(window).height();

    style.appendTo("head");
    styleLED.appendTo("head");

    new ScrollMagic.Scene({
            triggerElement: "#trigger-3",
            duration: .5 * vh,
            offset: .1 * vh
        })
        .setTween(TweenMax.to(parallax_object, 50, {
            className: "+=" + style_name
        }))
        .addTo(controller);

    new ScrollMagic.Scene({
            triggerElement: "#trigger-3",
            duration: .5 * vh,
            offset: .1 * vh
        })
        .setTween(TweenMax.to(parallax_led, 1, {
            className: "+=" + styleLED_name + " > circle"
        }))
        .addTo(controller);
}

function parallax_init() {
    "use strict";
    var parallax_object,
        style_name,
        style,
        parallax_led,
        styleLED_name,
        styleLED;

    $("<style type='text/css'> .hidden {opacity:0;transition:opacity .5s ease;} </style>").appendTo("head");

    parallax_object = "#parallax-object-1";
    style_name = "parallax-move-1";
    style = $("<style type='text/css'> ." + style_name + "{" +
        "top: calc(92% - " + $(parallax_object).outerHeight() + "px)" +
        ";} </style>");
    parallax_led = "#led-1";
    styleLED_name = "parallax-led-1";
    styleLED = $("<style type='text/css'> ." + styleLED_name + "{" +
        "fill: rgb(0, 0, 0)" +
        ";} </style>");

    set_parallax(parallax_object, style_name, style, parallax_led, styleLED_name, styleLED);

    parallax_object = "#parallax-object-2";
    style_name = "parallax-move-2";
    style = $("<style type='text/css'> ." + style_name + "{" +
        "top: calc(70% - " + $(parallax_object).outerHeight() + "px)" +
        ";} </style>");
    parallax_led = "#led-2";
    styleLED_name = "parallax-led-2";
    styleLED = $("<style type='text/css'> ." + styleLED_name + "{" +
        "fill: rgb(0, 120, 0)" +
        ";} </style>");

    set_parallax(parallax_object, style_name, style, parallax_led, styleLED_name, styleLED);

    parallax_object = "#parallax-object-3";
    style_name = "parallax-move-3";
    style = $("<style type='text/css'> ." + style_name + "{" +
        "top: calc(74% - " + $(parallax_object).outerHeight() + "px)" +
        ";} </style>");
    parallax_led = "#led-3";
    styleLED_name = "parallax-led-3";
    styleLED = $("<style type='text/css'> ." + styleLED_name + "{" +
        "fill: rgb(0,85,0)" +
        ";} </style>");

    set_parallax(parallax_object, style_name, style, parallax_led, styleLED_name, styleLED);

    parallax_object = "#parallax-object-4";
    style_name = "parallax-move-4";
    style = $("<style type='text/css'> ." + style_name + "{" +
        "top: calc(85% - " + $(parallax_object).outerHeight() + "px)" +
        ";} </style>");
    parallax_led = "#led-4";
    styleLED_name = "parallax-led-4";
    styleLED = $("<style type='text/css'> ." + styleLED_name + "{" +
        "fill: rgb(0,40,0)" +
        ";} </style>");

    set_parallax(parallax_object, style_name, style, parallax_led, styleLED_name, styleLED);

    parallax_object = "#parallax-object-5";
    style_name = "parallax-move-5";
    style = $("<style type='text/css'> ." + style_name + "{" +
        "top: calc(60% - " + $(parallax_object).outerHeight() + "px)" +
        ";} </style>");
    parallax_led = "#led-5";
    styleLED_name = "parallax-led-5";
    styleLED = $("<style type='text/css'> ." + styleLED_name + "{" +
        "fill: rgb(0,185,0)" +
        ";} </style>");

    set_parallax(parallax_object, style_name, style, parallax_led, styleLED_name, styleLED);


    var vh = $(window).height(),
        tween = TweenMax.to("#main-screen", 50, {
            backgroundPositionY: 30 + "vh",
            ease: Linear.easeNone,
            force3D: true
        });

    new ScrollMagic.Scene({
            triggerElement: "#trigger-1",
            duration: 1 * vh,
            offset: 0.5 * vh
        })
        .setTween(tween)
        .addTo(controller);

    tween = TweenMax.to("#info-block-parallax-1", 50, {
        top: -10 + "vh",
        ease: Linear.easeNone,
        force3D: true
    });

    new ScrollMagic.Scene({
            triggerElement: "#trigger-2",
            duration: 1.4 * vh,
            offset: -0.2 * vh
        })
        .setTween(tween)
        .addTo(controller);

    $('.hidden').each(function() {
        var current = this;

        new ScrollMagic.Scene({
                triggerElement: current,
                offset: -0.3 * vh
            })
            .setClassToggle(current, "showme")
            .reverse(false)
            .addTo(controller);
    });
};

$(parallax_init());